\documentclass{beamer}
%\usetheme{Warsaw}

\usetheme{Darmstadt}
\useoutertheme{infolines} 

\usepackage{graphicx, color, url, verbatim}
\usepackage{textpos,fancybox, multicol}
\usepackage{tikz}
\usepackage[T1]{fontenc}
\usetikzlibrary{arrows,shapes}
\usepackage{epigraph}
\usepackage{calc}

\newcommand{\mytextformat}{\itshape\epigraphsize}
\newenvironment{mytext}{\mytextformat}{}
\newenvironment{mysource}{\scshape\hfill}{}
\renewcommand{\textflush}{mytext} 
\renewcommand{\sourceflush}{mysource}

\let\originalepigraph\epigraph 
\renewcommand\epigraph[2]%
   {\setlength{\epigraphwidth}{\widthof{\mytextformat#1}}\originalepigraph{#1}{#2}}

\addtobeamertemplate{frametitle}{}{%
\begin{textblock*}{100mm}(.95\textwidth,-.9cm)
\includegraphics[height=0.8cm]{Figures/JHU/Dome}
\end{textblock*}}

\newcommand{\TODO}[1]{\textcolor{red}{#1}}


\title[Tumor dysregulation with EVA]{Uncovering hidden sources of transcriptional dysregulation arising from inter- and intra-tumor heterogeneity}
\author[@FertigLab]{Elana J Fertig}
\institute[Johns Hopkins University]{\includegraphics[width=1in]{Figures/JHU/SKCCCLogo}}
\date[UiO]{University of Oslo\\28 Feb 2019}

\begin{document}

\begin{frame}[plain]
\titlepage
\end{frame}

\begin{frame}[plain]
\frametitle{Overview}
\tableofcontents
\end{frame}

\section{Introduction to tumor heterogeneity}

\begin{frame}[plain]
\frametitle{Overview}
\tableofcontents[currentsection]
\end{frame}

\begin{frame}{What is tumor heterogeneity?}
\epigraph{Happy families are all alike; every unhappy family is unhappy in its own way.}{Leo Tolstoy, \emph{Anna Karenina}}

\end{frame}

\begin{frame}{Types of tumor heterogeneity}

\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/FrameEtAl.png}
\end{center}
\flushright{Frame et al., 2017, \emph{JCMT}}
\end{frame}

\begin{frame}{Head and neck cancer etiology and heterogeneity}
\begin{center}
\includegraphics[width=0.75\textwidth,page=3]{Figures/FiguresDraft1Final.pdf}
\end{center}
\begin{flushright}
Faraji et al., 2018, \emph{Molecular Determinants of Head and Neck Cancer}
\end{flushright}
\end{frame}

\begin{frame}{Mutations are insufficient to explain widespread transcriptional changes}
\begin{center}
\includegraphics[width=0.75\textwidth,page=4]{Figures/FiguresDraft1Final.pdf}
\end{center}
\begin{flushright}
Faraji et al., 2018, \emph{Molecular Determinants of Head and Neck Cancer}
\end{flushright}
E.g., \emph{EGFR} overexpressed in 90\% of HNSCC and associated with poor prognosis, but has a low mutation rate in HNSCC.
\end{frame}

\begin{frame}{Tumor heterogeneity impacts treatment selection and biomarker development}
\begin{itemize}
\item Population level variation within a disease subtype causes individual patients to benefit from different therapies.
\item Increased molecular heterogeneity is associated with poor prognosis.
\item Regional variation in tumors will render clinical decisions sensitive to the location at which a biopsy is obtained.
\item Cellular heterogeneity gives rise to therapeutic resistance.
\end{itemize}
\end{frame}

\begin{frame}{Cellular, genomic, and epigenetic states can be measured with bulk and single cell genomics technologies}

\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/FrameEtAlHighlightTranscription.pdf}
\end{center}
\begin{flushright}
Frame et al., 2017, \emph{JCMT}
\end{flushright}

\end{frame}

\begin{frame}{Heterogeneity changes transcription in multiple genes}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/RegevVariation.jpg}
\end{center}
\begin{flushright}
Wagner et al., 2016, \emph{Nature Biotech}
\end{flushright}

\end{frame}

\section{Hypothesis testing for relative heterogeneity}

\begin{frame}[plain]
\frametitle{Overview}
\tableofcontents[currentsection]
\end{frame}


\begin{frame}{New statistical methods are needed to quantify heterogeneity changes transcription in multiple genes}
\begin{columns}
\column{0.5\textwidth}

\begin{center}
\includegraphics[width=\textwidth]{Figures/RegevVariation.jpg}
\end{center}

\begin{flushright}
Wagner et al., 2016, \emph{Nature Biotech}
\end{flushright}

\column{0.5\textwidth}
\textbf{Current analysis methods}
\begin{itemize}
\item Visualization without statistical quantification (e.g., clustering, UMAP, PCA, etc).
\item Mean shifts in transcription between sample groups (e.g., differential expression, gene set analysis, etc).
\item Univariate, gene-centric outlier statistics.
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Adapting the Anna Karenina principle to genomics}



\epigraph{Happy families are all alike; every unhappy family is unhappy in its own way.}{Leo Tolstoy, \emph{Anna Karenina}}

\begin{columns}
\column{0.4\textwidth}
\begin{center}
\includegraphics[width=\textwidth]{Figures/EVAConcept.pdf}
\end{center}
\column{0.6 \textwidth}
\textbf{Expression Variation Analysis (EVA)} compares the dissimilarity of transcriptional profiles from the blue group (normal) to the red group (tumors).

\begin{center}
\includegraphics[width=0.2\textwidth]{Figures/bafsari1.jpg}
\textbf{Bahman Afsari} 

\end{center}



\end{columns}

\begin{center}
Afsari et al., 2014, \emph{Cancer Informatics}; R/Bioconductor GSReg
\end{center}

\end{frame}

\begin{frame}{Expression Variation Analysis (EVA)}
\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/EVAAlgorithm.pdf}

Afsari et al., 2014, \emph{Cancer Informatics};\\R/Bioconductor GSReg.
\end{center}
\end{frame}

\begin{frame}{U-theory statistics can compare expected dissimilarities}
\begin{center}
\includegraphics[width=0.7\textwidth]{Figures/EVAAlgorithmUTheory.pdf}

Afsari et al., 2014, \emph{Cancer Informatics}; R/Bioconductor GSReg
\end{center}
\end{frame}

\begin{frame}{EVA statistics depend on the dissimilarity measure}
\begin{columns}
\column{0.5\textwidth}
\begin{center}
\includegraphics[width=\textwidth]{Figures/KendalTau.pdf}
\end{center}


\column{0.5\textwidth}
\textbf{Kendall-tau dissimilarity}
\begin{itemize}
\item Compares the similarity of rank orderings.
\item Independent of the overall expression levels for each sample.
\item Robust to data normalization and measurement platform.
\end{itemize}
\end{columns}
\begin{center}
Afsari et al., 2014, \emph{Cancer Informatics}; R/Bioconductor GSReg
\end{center}
\end{frame}

\begin{frame}{EVA confirms greater pathway dysregulation of HNSCC tumors than normal samples}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/EVAAnalysis.pdf}
\end{center}
EVA analysis of RNA-seq data for HNSCC tumors and normal samples from TCGA to quantify dysregulation in MSigDB C2 canonical pathways.
\begin{flushright}
Faraji et al., 2018, \emph{Molecular Determinants of Head and Neck Cancer}
\end{flushright}
\end{frame}


\section{Transcriptional regulation in head and neck cancer}

\begin{frame}[plain]
\frametitle{Overview}
\tableofcontents[currentsection]
\end{frame}

\subsection{Epigenetic regulation}

\begin{frame}{Mutations are insufficient to explain widespread transcriptional changes}
\begin{center}
\includegraphics[width=0.75\textwidth,page=4]{Figures/FiguresDraft1Final.pdf}
\end{center}
\begin{flushright}
Faraji et al., 2018, \emph{Molecular Determinants of Head and Neck Cancer}
\end{flushright}
E.g., \emph{EGFR} overexpressed in 90\% of HNSCC and associated with poor prognosis, but has a low mutation rate in HNSCC.
\end{frame}

\begin{frame}
\frametitle{DNA methylation is significantly altered in HNSCC}
\begin{columns}[T]

\column{0.5\textwidth}

\textbf{Global hypomethylation correlates with HNSCC tumor stage}


\begin{center}

%\includegraphics[width=\textwidth]{Background/CalifanoMeth} 
\includegraphics[width=\textwidth]{Figures/stagedot}
 
 Smith et al. (2007) \textit{Int J Cancer}
\end{center}
\column{0.5\textwidth}

\textbf{In HNSCC, methylation changes have been associated with}

\begin{itemize}
\item HPV status \\ {\footnotesize{Richards et al., 2009, \textit{PLoS One}; \\Park et al., 2011, \textit{Cancer Prev Res} \\ Sartor et al, 2011, \textit{Epigenetics}}}
\item \textit{In vitro} response to EGFR inhibitors \\ {\footnotesize{Ogawa et al, 2012, \textit{Cell Cycle}}}
\item Pretreatment dietary intake \\ {\footnotesize{Colacino et al., 2012, \textit{Epigenetics}}}
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Epigenetic regulation confers potential for transcription}
\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/EpigeneticReg}
 \end{center}

\begin{flushright}
Kagohara et al et al. (2018) \textit{Brief Funct Genomics}
\end{flushright}
\end{frame}

\begin{frame}{ChIP-seq and RNA-seq for HNSCC primary tumors}
\begin{columns}[T]
\column{0.5\textwidth}
\begin{center}
\includegraphics[width=\textwidth]{Figures/ChIPSeqData.pdf}
\end{center}
\vspace{-14pt}
\begin{itemize}
\item 2 HPV+ cell lines
\item 2 patient derived xenografts from HPV+ HNSCC tumors 
\item 2 normal samples 
\end{itemize}
\vspace{-14pt}
\begin{flushright}
Kelley et al., 2017, \emph{Cancer Research}
\end{flushright}
\column{0.5\textwidth}
\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/ChEVARNA.pdf}
\end{center}

RNA-seq on 47 HPV+ HNSCC tumor and 25 additional normal samples
\vspace{8pt}
\begin{flushright}
Guo et al., 2016, \emph{Int J Cancer}
\end{flushright}
\end{columns}
\end{frame}

\begin{frame}{EVA adapts to integrate ChIP-seq and RNA-seq data}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/CHEVAFigure.pdf}
\end{center}
\begin{flushright}
Kelley et al., 2017, \emph{Cancer Research}
\end{flushright}
\end{frame}

\begin{frame}{EVA confirms that enhancer regulation is associated with tumor heterogeneity and HPV-integration}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/ChEVARNA.pdf}
\end{center}
\begin{flushright}
Kelley et al., 2017, \emph{Cancer Research}
\end{flushright}
\end{frame}

\subsection{Alternative splicing}

\begin{frame}{Splicing alterations also driver cancer}
\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/EpigeneticSplicing.jpg}
\end{center}
\begin{flushright}
Narayanan et al., 2017, \emph{Biochemical Journal}
\end{flushright}
\end{frame}

\begin{frame}{SEVA models splicing dysregulation}
\begin{center}
\includegraphics[width=0.75\textwidth]{Figures/SEVA.pdf}
\end{center}

\begin{flushright}
Afsari et al., 2018, \emph{Bioinformatics}
\end{flushright}
\end{frame}

\begin{frame}{SEVA models splicing dysregulation}
\begin{center}
\includegraphics[width=0.9\textwidth]{Figures/SEVA2.pdf}
\end{center}

\begin{flushright}
Afsari et al., 2018, \emph{Bioinformatics}
\end{flushright}
\end{frame}

\begin{frame}{SEVA confirms previously identified splice variants in HNSCC}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/Figure4MDSPlots.pdf}
\end{center}
\begin{flushright}
Afsari et al., 2018, \emph{Bioinformatics}
\end{flushright}
\end{frame}


\begin{frame}{SEVA finds greater variation in splice dysregulation in HPV- HNSCC tumors with mutations in splicing genes}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/SEVAFig5.pdf}
\end{center}
\begin{flushright}
Afsari et al., 2018, \emph{Bioinformatics}
\end{flushright}
\end{frame}

\section{Single cell analysis}

\begin{frame}[plain]
\frametitle{Overview}
\tableofcontents[currentsection]
\end{frame}

\begin{frame}{Single cell data provides new insights into heterogeneity}
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/UMAP.pdf}
\end{center}
\begin{flushright}
Davis-Marcisak et al., 2018, \emph{BioRxiv}
\end{flushright}
\end{frame}

\begin{frame}{Imputation removes biases introduced by zeros}
\begin{center}
\includegraphics[width=0.7\textwidth]{Figures/evascImpute.pdf}
\end{center}
\begin{flushright}
Davis-Marcisak et al., 2018, \emph{BioRxiv}
\end{flushright}
\end{frame}

\begin{frame}{Public domain single cell dataset of HNSCC provides new opportunities to look at heterogeneity of tumor cells}
\begin{center}
\includegraphics[width=\textwidth]{Figures/RegevHNSCCFig1.jpg}
\end{center}
\begin{flushright}
Puram et al., 2018, \emph{Cell}
\end{flushright}
\end{frame}

\begin{frame}{EVA identifies differences between cancer cells in primary tumors and lymph node metastasis not in clustering}
\begin{columns}
\column{0.5\textwidth}
\begin{center}

\includegraphics[width=\textwidth]{Figures/HNSCCMets.jpg}
\end{center}
\begin{flushright}
Puram et al., 2018, \emph{Cell}
\end{flushright}
\column{0.5\textwidth}
\begin{center}
\includegraphics[width=\textwidth]{Figures/EVAMetastasis.pdf}
\end{center}
\begin{flushright}
Davis-Marcisak et al., 2018, \emph{BioRxiv}
\end{flushright}

\end{columns}
\end{frame}

\section{Conclusions}

\begin{frame}{EVA provides novel insights into transcriptional regulation of HNSCC}
\begin{itemize} 
\item There is greater pathway dysregulation in tumor than normal samples.
\item Enhancer modifications are associated with transcriptional heterogeneity, reflective of their potential for activity in their mechanism of action.
\item Tumor-specific changes in H3K27ac are associated with HPV viral integration in HNSCC.
\item Mutations in splicing machinery in HPV- HNSCC tumors are associated with splicing dysregulation. 
\item Increased transcriptional heterogeneity in primary cancer cells than in lymph node metastases supports a clonal outgrowth model. 
\end{itemize}
\end{frame}

\begin{frame}{EVA is a robust statistical framework for differential heterogeneity analysis}
\begin{itemize}
\item EVA provides a robust statistical framework for differential heterogeneity analysis.
\item The rank-based statistics are robust to data normalization, enabling widespread application across measurement technologies.
\item We demonstrate the utility of EVA for pathway dysregulation, epigenetic regulation, splicing, and single cell analysis.
\item Recent extensions of EVA to continuous phenotypes enable association with therapeutic outcomes in screening studies (Afsari et al., 2018, BioRxiv) and to developmental biology (Clark et al., 2018, BioRXiv).
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Acknowledgements}

\begin{columns}[T]
\column{0.5\textwidth}
\textbf{Johns Hopkins University}

\begin{itemize}
\item Fertig Lab (@FertigLab)
\begin{itemize}
\item \textbf{Bahman Afsari}
\item \textbf{Emily Davis-Marcisak}
\end{itemize}
\item Seth Blackshaw
\item Leslie Cope
\item Alexander V. Favorov
\item Daria A. Gaykalova
\item Donald Geman
\item Loyal A. Goff
\item Elizabeth M. Jaffee
\begin{itemize}
\item Evanthia Russos-Torres
\item Alexander Hopkins
\end{itemize}
\end{itemize}



\column{0.5\textwidth}
\textbf{The College of New Jersey}
\begin{itemize}
\item Michael F Ochs
\end{itemize}

\textbf{USCD}
\begin{itemize}
\item Joseph Califano
\begin{itemize}
\item Theresa Guo (JHU)
\end{itemize}
\end{itemize}

\textbf{UCSF}
\begin{itemize}
\item Patrick Ha
\end{itemize}

\textbf{Washington University}
\begin{itemize}
\item Brian Clark
\item Sid Puram
\end{itemize}

\begin{center}
\includegraphics[width=0.9\textwidth]{Figures/Funding/Funding.pdf}
\end{center}

\end{columns}
\end{frame}

\end{document}
